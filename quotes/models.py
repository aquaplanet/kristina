# -*- coding: utf-8 -*-
from django.db import models

# New for me: Use this to get today's date, it is timezone aware!
from django.utils import timezone
from django.forms import ModelForm, CharField, Textarea, TextInput
from random import choice


class UnusedManager(models.Manager):
    def get_query_set(self):
        return super(UnusedManager, self).get_query_set().filter(
            used__isnull=True)
    def random(self):
        q = super(UnusedManager, self).get_query_set().filter(
            used__isnull=True)
        if not q:
            return None
        return choice(q)
        
class UsedManager(models.Manager):
    def get_query_set(self):
        return super(UsedManager, self).get_query_set().filter(
            used__isnull=False)

# Create your models here.
class Quote(models.Model):
    """A model to hold text quotes, that is it

    It has fields:
    quote - The quote to store.
    used  - None if not used otherwise a datetime when you flaged it used last time.
    source - A CharField to hold the source reference."""

    # This is how many characters to display of the quote when displaying a short bit
    MAX_LENGTH=15
    objects = models.Manager()
    used_objects = UsedManager()
    unused_objects = UnusedManager()

    quote = models.TextField(max_length=2000,
                             help_text='The memorable peice of text to store.',
                             unique=True)
    used = models.DateTimeField(blank=True,
                                null=True,
                                default=None,
                               help_text='Track when you used this quote the last time.')
    source = models.CharField(max_length=1000,
                              blank=True,
                              default='',
                              help_text='Where did you get this text?')

    def short_quote(self):
        """If you only need a few chars from the quote"""

        if len(self.quote) <= self.MAX_LENGTH:
            return self.quote
        else:
            return self.quote[:12]+'...'

    def __unicode__(self):
        return self.short_quote()

    def set_used(self, save=True):
        """Mark the quote used and save it per default, save=True"""

        self.used = timezone.now()
        if save:
            self.save()

    def set_unused(self, save=True):
        """Mark the quote used and save it per default, save=True"""

        self.used = None
        if save:
            self.save()

    def __unicode__(self):
        """To display the quote object properly, I assume that is just to display the quote"""

        return self.quote

    def save(self, *args, **kwargs):
        # You can't timetravel into the future
        if self.used and self.used > timezone.now():
            self.used = timezone.now()

        super(Quote, self).save(*args, **kwargs)

class QuoteForm(ModelForm):
    quote = CharField(
        label="",
        max_length=2000,
        widget=Textarea(attrs={'class':'input-block-level',
                               'placeholder':'Skriv något vackert till Kristina'}))
    source = CharField(
        label="Källhänvisning",
        required = False,
        widget=TextInput(attrs={'class':'input-block-level',
                                'placeholder':'Vill du hänvisa Kristina till källan av de vackra orden kan du skriva in det här'}))
    
    class Meta:
        fields = ('quote','source',)
        model = Quote
