from django import template
from quotes.models import Quote
import re

register = template.Library()

@register.simple_tag
def print_quote(id):
    if type(id) == Quote:
        q = id
    else:
        try:
            q = Quote._default_manager.get(pk=int(id))
        except Quote.DoesNotExist:
            return u"Quote does not exist"

    q_text = u"</p>\n<p>".join(re.split(r"[\r\n\f\v]+", q.quote))

    if q.source:
         q_text = u"<blockquote><p>{quote}</p>" \
                  u"<footer>- {link}</footer>" \
                  u"</blockquote>".format(quote=q_text, link=q.source)
    else:
         q_text = u"<blockquote><p>{}</p></blockquote>".format(q_text)


    return q_text
    
