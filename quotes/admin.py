from django.contrib import admin
from quotes.models import Quote
from django.utils import timezone

def marked_used(modeladmin, request, queryset):
    """Admin Action: Mark all selected quotes as used by setting todays date and time"""

    queryset.update(used=timezone.now())
marked_used.short_description = "Mark as used today"

def marked_unused(modeladmin, request, queryset):
    """Admin Action: Unmark all selected quotes by setting the used date to None"""
    
    queryset.update(used=None)
marked_unused.short_description = "Pretend these weren't used"

class QuoteAdmin(admin.ModelAdmin):
    
    # No Need to go overboard on all fields to display
    list_display = ('short_quote','used',)

    # One might want to sort on the datetime fields
    list_filter = ('used',)

    # We want to search a quote, don't we
    search_fields = ('quote',)

    # Actions on the quotes, we want to select them as used and unused
    actions = (marked_used,marked_unused,)

admin.site.register(Quote, QuoteAdmin)
