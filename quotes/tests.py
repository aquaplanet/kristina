#-*- coding: utf-8 -*-
"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from quotes.models import Quote
from django.utils import timezone
from datetime import timedelta

class QuoteTestCase(TestCase):
    def setUp(self):
        qs = [Quote(quote="Kristina är den finaste isprinsessan som finns på denna jord"),
              Quote(quote="Kristina är snygg"),
              Quote(quote="""Rosor är röda,
violer är blå,
emacs och vim är fina,
Kristina lika så!"""),
              Quote(quote="Detta är redan använt...Kristina jag har inget att följa upp med " \
                        "här. Du använder din hjärna hela tiden men den är inte färdiganvänd " \
                        "heller...det kommer den aldrig att vara",
                    used=timezone.now()),
              ]

        map(lambda x: x.save(), qs)             
        self.number_of_quotes = len(qs)

        # Do not trust the qs-variable. Fetch them from the database
        # just in case (load might be overloaded?)
        self.all_quotes = Quote.objects.all()

    def tearDown(self):
        self.all_quotes.delete()
        
    def test_number_of_quotes(self):
        """
        I test if all the quotes are returned.
        """
        self.assertEqual(self.number_of_quotes, len(self.all_quotes),
                         msg="fetched not the same amount of quotes as we saved")

    def test_short_quote(self):
        """
        Test the short_quote method which are supposed to return Quote.MAX_LENGTH
        number of characters including ...
        """
        for q in self.all_quotes:
            self.assertEqual(len(q.short_quote()), Quote.MAX_LENGTH)
            self.assertEqual(q.short_quote()[-3:], "...",
                             msg="Atleast one quote does not en with ...")
   
    def test_used_in_the_future(self):
        """
        No quotes may be used in the future...
        Save a quote used in the future (1 day) and check if it is in the future
        """
        q = Quote(quote="Future", used=timezone.now()+timedelta(days=1))
        q.save()

        # If used is in the future this will be negative
        dt = (timezone.now() - q.used).total_seconds()
        self.assertGreaterEqual(dt, 0,
                                msg="The quote is %F sec in the future" % (dt*-1))
