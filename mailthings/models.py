#-*- coding: utf-8 -*-

from django.db import models
from django.forms import ModelForm, CharField, PasswordInput, \
    EmailField, IntegerField, BooleanField, TextInput
from django.core.mail import EmailMultiAlternatives, get_connection
from django.conf import settings
import re

# Create your models here.
class Settings(models.Model):
    sender_mail = models.EmailField(
        max_length=200,
        help_text="Avsändarens adress"
        )
    server_username = models.CharField(
        max_length=200,
        help_text="Användaren på epostservern"
        )
    server_password = models.CharField(
        max_length=200,
        help_text="Lösenrodet på epostservern"
        )
    server_address = models.CharField(
        max_length=200,
        help_text="Epostserverns adress"
        )
    server_port = models.PositiveSmallIntegerField(
        help_text="Epostserverns port (normalt 587)"
        )
    server_tls = models.BooleanField(
        help_text="Använd TLS-anslutning?"
        )
    receiver_address = models.CharField(
        max_length=2000,
        help_text="Kommaseparerad lista av epost-adresser att "\
            "normalt skicka till"
        )
    default_subject = models.CharField(
        max_length=200,
        help_text="Förifylld ämnesrad"
        )

class SettingsForm(ModelForm):
    sender_mail=EmailField(
        label='Avsändarens epostadress',
        max_length=200,
        widget=TextInput(attrs={'class':'input-xxlarge'})
        )
    server_username=CharField(
        label='Användarnamn på epostservern',
        max_length=200,
        widget=TextInput(attrs={'class':'input-xxlarge'})
        )
    server_password=CharField(
        label='Lösenordet på epostservern',
        max_length=200,
        widget=TextInput(attrs={'class':'input-xxlarge'})
        )
    server_address=CharField(
        label="Epostserverns adress",
        max_length=200,
        widget=TextInput(attrs={'class':'input-xxlarge'})
        )
    server_port=IntegerField(
        label="Epostserverns port",
        widget=TextInput(attrs={'class':'input-xxlarge'})
        )
    server_tls=BooleanField(label="Använd TLS?")
    receiver_address=CharField(
        label="Mottagaradresser (kommaseparerade)",
        widget=TextInput(attrs={'class':'input-xxlarge'}),
        max_length=2000
        )
    default_subject=CharField(
        label="Förifylld ämnesrad för nya brev",
        max_length=200,
        widget=TextInput(attrs={'class':'input-xxlarge'})
        )

    class Meta:
        model = Settings



def mail_html(subject, text_contents, html_contents, to=[]):
    # Get and setup our settings first
    mail_settings = Settings._default_manager.get(pk=1)

    backend = get_connection()
    backend.host = mail_settings.server_address
    backend.port = mail_settings.server_port
    backend.username = mail_settings.server_username
    backend.password = mail_settings.server_password
    backend.use_tls = mail_settings.server_tls

    if not to:
        to = re.split(r'\s*,\s*', mail_settings.receiver_address)

    if type(to) == type('string') and ',' in to:
        to = re.split(r'\s*,\s*', to)
    else:
        to = [to]


    msg = EmailMultiAlternatives(subject,
                                 text_contents,
                                 mail_settings.sender_mail,
                                 to,
                                 connection=backend)

    msg.attach_alternative(html_contents, "text/html")

    msg.send()
