# Create your views here.
from django.views.generic import UpdateView
from mailthings.models import Settings, SettingsForm

class UpdateSettings(UpdateView):
    form_class = SettingsForm
    model = Settings
    
    
