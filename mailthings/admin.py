from django.contrib import admin
from mailthings.models import Settings

class SettingsAdmin(admin.ModelAdmin):
    pass
admin.site.register(Settings, SettingsAdmin)

