from django.contrib import admin
from kristina.models import Kristina
from django.utils import timezone

def set_unsent(modeladmin, request, queryset):
    queryset.update(sent=None)
set_unsent.short_description = u"Ange markerade som oskickade brev"

def set_sent(modeladmin, request, queryset):
    queryset.update(sent=timezone.now())
set_sent.short_description = u"Ange markerade som att de skickades idag"

def remove_kristina(modeladmin, request, queryset):
    for q in queryset:
        q.delete()
remove_kristina.short_description = "Tabort markerade brev"

class KristinaAdmin(admin.ModelAdmin):
    # No Need to go overboard on all fields to display
    list_display = ('title','get_title','sent',)

    # One might want to sort on sent date
    list_filter = ('sent',)

    # We want to search a video title and url, don't we
    search_fields = ('title','video__title','sent',)

    # Actions on the videos, we want to select them as used and unused
    actions = (set_unsent, set_sent,remove_kristina,)

    def get_title(self, obj):
        return "%s" % (obj.video.title)

    get_title.short_description = 'Title'

    def get_actions(self, request):
        actions = super(KristinaAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions



admin.site.register(Kristina, KristinaAdmin)
