from django.shortcuts import render
from django.core.urlresolvers import reverse_lazy
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView
from kristina.models import Kristina, KristinaForm
from quotes.models import Quote, QuoteForm
from video.models import Video, VideoForm
from mailthings.views import UpdateSettings
from mailthings.models import mail_html, Settings
from django.utils import timezone
from django.template.loader import render_to_string
from random import randint
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

class WriteView(CreateView):
    form_class = KristinaForm
    template_name='kristina/write.html'
    success_url = reverse_lazy('write')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(WriteView, self).dispatch(*args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super(WriteView, self).get_context_data(**kwargs)
        context['active'] = 'write'
        return context

    def get_initial(self, *args, **kwargs):
        initial = super(WriteView, self).get_initial(*args, **kwargs)

        mail_settings = Settings._default_manager.get(pk=1)
        initial['from_addr'] = mail_settings.sender_mail
        initial['sent'] = timezone.now()
        initial['title'] = mail_settings.default_subject
        initial['to_addr'] = mail_settings.receiver_address
        initial['quote'] = Quote.unused_objects.random()
        initial['video'] = Video.unused_objects.random()
        return initial

    def form_valid(self, form):
        template_vars = {
            'video': form.cleaned_data['video'],
            'quote': form.cleaned_data['quote'],
            'more_text': form.cleaned_data['more_text'],
            }

        text_contents = render_to_string('kristina/text_mail.txt', template_vars)
        html_contents = render_to_string('kristina/html_mail.html', template_vars)
            
        subject = form.cleaned_data['title']
        to = form.cleaned_data['to_addr']
        
        mail_html(subject, text_contents, html_contents, to)
        return super(WriteView, self).form_valid(form)

class ComplimentsView(CreateView):
    template_name='kristina/compliments.html'
    success_url = reverse_lazy('compliments')
    model = Quote
    form_class = QuoteForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ComplimentsView, self).dispatch(*args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super(ComplimentsView, self).get_context_data(**kwargs)
        context['active'] = 'compliments'
        context['quotes'] = Quote.unused_objects.all()
        return context

class VideoView(CreateView):
    template_name='kristina/video.html'
    success_url = reverse_lazy('video')
    model = Video
    form_class = VideoForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(VideoView, self).dispatch(*args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super(VideoView, self).get_context_data(**kwargs)
        context['active'] = 'video'
        context['videos'] = Video.unused_objects.all()
        return context

class HistoryView(TemplateView):
    template_name='kristina/history.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HistoryView, self).dispatch(*args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super(HistoryView, self).get_context_data(**kwargs)
        context['active'] = 'history'
        context['lovestories'] = Kristina._default_manager.filter(
            sent__isnull=False).order_by('-sent');
        return context

class SettingsView(UpdateSettings):
    def get_context_data(self, **kwargs):
        context = super(SettingsView, self).get_context_data(**kwargs)
        context['active'] = 'settings'
        return context

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(SettingsView, self).dispatch(*args, **kwargs)
    

        
