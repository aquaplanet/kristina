from django.conf.urls import patterns, include, url
from django.core.urlresolvers import reverse_lazy
from kristina.views import *

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$', WriteView.as_view(), name='write'),
    url(r'^compliments$', ComplimentsView.as_view(), name='compliments'),
    url(r'^video$', VideoView.as_view(), name='video'),
    url(r'^history$', HistoryView.as_view(), name='history'),

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^accounts/', include('accounts.urls')),
    
# Not protected 
#    url(r'^api/', include('kristina.api.urls')),

    url(r'^settings$',
        SettingsView.as_view(
            success_url=reverse_lazy('settings'),
            template_name='kristina/settings.html',
            ),
        {'pk':1},
        name="settings"),
)
