from django import template
from django.shortcuts import get_object_or_404
from kristina.models import Kristina
import re

register = template.Library()

@register.simple_tag
def print_text(id):
    try:
        k = Kristina._default_manager.get(pk=int(id))
    except Kristina.DoesNotExist:
        return u'Wrong id for Kristina'

    k_text = u'</p>\n<p>'.join(re.split(r'[\r\n\f\v]+', k.more_text))
    return u'<p>%s</p>' % (k_text)

@register.inclusion_tag('kristina/tag_addresses.html')
def print_addresses(id):
    k = get_object_or_404(Kristina, pk=int(id))
    return {'from_addr': k.from_addr,
            'to_addr': re.split('\s*,\s*', k.to_addr),
            }

