from piston.handler import BaseHandler
from video.models import Video
from quotes.models import Quote
from kristina.models import Kristina

class VideoHandler(BaseHandler):
    allowed_methods = ('GET','POST','PUT','DELETE',)
    model = Video

    

class QuoteHandler(BaseHandler):
    allowed_methods = ('GET','POST','PUT','DELETE',)
    model = Quote

    
class KristinaHandler(BaseHandler):
    allowed_methods = ('GET','POST','PUT','DELETE',)
    model = Kristina
