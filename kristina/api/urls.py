from django.conf.urls.defaults import *
from piston.resource import Resource
from kristina.api.handlers import VideoHandler, QuoteHandler, \
    KristinaHandler

video_handler = Resource(VideoHandler)
quote_handler = Resource(QuoteHandler)
kristina_handler = Resource(KristinaHandler)

urlpatterns = patterns('',
    url(r'^video/(?P<id>\d+)/', video_handler, name="video_id"),
    url(r'^video/$', video_handler, name="video_all"),
    url(r'^quote/(?P<id>\d+)/', quote_handler, name="quote_id"),
    url(r'^quote/$', quote_handler, name="quote_all"),
    url(r'^kristina/(?P<id>\d+)/', kristina_handler, name="kristina_id"),
    url(r'^kristina/$', kristina_handler, name="kristina_all"),
    )
