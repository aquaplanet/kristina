#-*- coding: utf-8 -*-
from django.db import models
from quotes.models import Quote
from video.models import Video
from django.utils import timezone
from django.forms import ModelForm, TextInput, ModelChoiceField, Select, \
    Textarea, CharField, DateTimeInput, DateTimeField

class Kristina(models.Model):
    title = models.CharField(max_length=100,
                             blank=False)
    from_addr = models.CharField(max_length=100,
                                 blank=False)
    to_addr = models.CharField(max_length=1000,
                               blank=False)
    quote = models.OneToOneField(Quote,
                          blank=True,
                          null=True)
    video = models.OneToOneField(Video,
                          blank=True,
                          null=True)
    more_text = models.TextField(blank=True,
                          max_length = 1000)
    sent = models.DateTimeField(null = True,
                         blank = True,
                         default = None)
    def __unicode__(self):
        return self.title
    
    def save(self, *args, **kwargs):
        if self.quote and not self.quote.used:
            self.quote.used = timezone.now()
            self.quote.save()
        
        if self.video and not self.video.used:
            self.video.used = timezone.now()
            self.video.save()

        super(Kristina, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        if self.quote:
            self.quote.used = None
            self.quote.save()

        if self.video:
            self.video.used = None
            self.video.save()

        super(Kristina, self).delete(*args, **kwargs)

class KristinaForm(ModelForm):
    class Meta:
        model = Kristina

    title=CharField(
        label='Ämne',
        max_length=100,
        widget=TextInput(attrs={'class':'input-block-level',
                                'placeholder':'Skriv in en vacker strof'}))
    to_addr=CharField(
        label='Till',
        max_length=1000,
        widget=TextInput(attrs={'class':'input-block-level',
                                'placeholder':'Kristinas adress'}))
    quote=ModelChoiceField(
        label="Komplimang",
        queryset=Quote.unused_objects.all(),
        empty_label=None)
    video=ModelChoiceField(
        label="Film",
        queryset=Video.unused_objects.all(),
        empty_label=None)
    more_text=CharField(
        label="Meddelande",
        max_length=1000,
        widget=Textarea({'placeholder':"Skriv vad som har hänt under dagen "
                         "eller något annat som är värt att berätta",
                         'class':'input-block-level', 'rows':10}))
    # Hidden fields
    sent=DateTimeField(
        widget=DateTimeInput(attrs={'style':'display: none'}))
    from_addr=CharField(
        widget=TextInput(attrs={'style':'display: none'}))
