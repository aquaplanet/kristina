# -*- coding: utf-8 -*-
from django.contrib.auth.forms import AuthenticationForm
from django.forms import CharField, TextInput, PasswordInput


class MyAuthForm(AuthenticationForm):
    username = CharField(
        label = 'Användarnamn',
        max_length=254,
        widget=TextInput(attrs={'class':'input-block-level',
                                'placeholder':'Användarnamn'}))
    password = CharField(
        label= "Lösenord",
        widget=PasswordInput(attrs={'class':'input-block-level',
                                    'placeholder':'Lösenord'}))



