from django import template
from django.shortcuts import get_object_or_404
from video.models import Video
import video.tools as tools


register = template.Library()

@register.simple_tag
def embedd_video(id):
    thing_to_return = u''

    if type(id) == Video:
        v = id
    else: 
        v = get_object_or_404(Video, pk=int(id))

    if tools.is_youtube(v.url):
        thing_to_return = u'<iframe width="560" height="315" src="http://www.youtube.com/embed/%s" frameborder="0" allowfullscreen></iframe>' % (tools.youtube_get_v(v.url))
    elif tools.is_vimeo(v.url):
        thing_to_return = u'<iframe src="http://player.vimeo.com/video/%s?portrait=0" width="500" height="281" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>' % (tools.vimeo_get_v(v.url))
    else: 
        thing_to_return = u"<a href='%s'>%s</a>" % (v.url, v.title)

    if v.comment != "":
        thing_to_return = u'<p>%s</p><blockquote style="font-style: italic">%s</blockquote>' % \
            (thing_to_return, v.comment)
    return thing_to_return

@register.simple_tag
def link_video_title(id):
    thing_to_return = u''


    if type(id) == Video:
        v = id
    else: 
        v = get_object_or_404(Video, pk=int(id))

    thing_to_return = u'<a href="%s">%s</a>' % (v.url, v.title)
    return thing_to_return

@register.simple_tag
def present_video(id):
    if type(id) == Video:
        v = id
    else: 
        v = get_object_or_404(Video, pk=int(id))

    embedd = embedd_video(v)
    link = link_video_title(v)
    return u"<p>%s</p><p>%s</p>" % (embedd, link)
