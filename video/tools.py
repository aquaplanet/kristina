#-*- coding: utf-8 -*-
import urllib2
import urlparse
import html5lib
from lxml import etree
import re

def link_title(url):
    """
    Returns the title of the page the url is pointing at
    """
    # Fetch the page, if it goes wrong raise an exception
    # we don't want broken urls
    # luckilty urllib2 will return URLError
    f_url = urllib2.urlopen(url)

    # More than I understand
    doc = html5lib.parse(f_url, treebuilder='lxml')
    root = doc.getroot()
    nsmap = {'html': "http://www.w3.org/1999/xhtml"}
    title = root.xpath('//html:title/text()', namespaces=nsmap)
        
    # Didn't find any title? Return the URL, better something than nothing
    if not title:
        return url
    
    return title[0]

################################################################################
# YOUTUBE
################################################################################
class InvalidYouTubeLink(Exception):
    def __init__(self, value):
        self.value = value
    def __unicod__(self):
        return repr(self.value)

def youtube_clean_title(title):
    """
    Make the title more appealing, that is removes YouTube branding and spacing
    """
    if title is None:
        return None

    t = re.sub(r"\s+-\s+YouTube", "", title)
    t = re.sub(r"\s+", " ", t)
    return t.strip()

def youtube_url_cleaner(url):
    """
    Youtube are using urls to track their users (GET requests). This
    cleans it up so we only save v=.... which is a pointer to the
    actual video.
    """
    if not is_youtube(url):
        raise InvalidYouTubeLink(url)
    
    return "http://www.youtube.com/watch?v=%s" % (youtube_get_v(url))

def youtube_get_v(url):
    """
    All youtube urls with video has a v=xyz where xyz is a random
    letter combination. This is the video name in google's
    database. This function returns that name.
    """
    if not is_youtube(url):
        raise InvalidYouTubeLink(url)
    
    parts = urlparse.urlparse(url)

    # Handling youtu.be urls
    if parts.netloc =='youtu.be':
        if parts.path and parts.path[0] == '/' and len(parts.path)>= 10:
            return parts.path[1:]
        else:
            raise InvalidYouTubeLink(url + " is not a video link")

    qs_parts = urlparse.parse_qs(parts.query)

    if 'v' in qs_parts:
        return qs_parts['v'][0]
    else:
        raise InvalidYouTubeLink(url + " doesn't contain v, not a video link")
    

def is_youtube(url):
    """
    Returns true if url is for a youtube video
    """
    if url is None:
        return False

    # Don't accept something.youtube.com, don't be lazy and have
    # "youtube.com" in y.
    test = lambda y: "youtube.com" == y or "www.youtube.com" == y or \
        "youtu.be" == y
    parts = urlparse.urlparse(url)

    # www.youtube.com has netloc='' and path='www.youtube.com'
    if parts.netloc == '':
        return test(parts.path)
    else:
        return test(parts.netloc)
    

################################################################################
# VIMEO
################################################################################
class InvalidVimeoLink(Exception):
    def __init__(self, value):
        self.value = value
    def __unicod__(self):
        return repr(self.value)

# TODO: Fixa URLEncodingWarningen
def is_vimeo(url):
    if url is None:
        return False

    # Don't accept something.youtube.com, don't be lazy,
    # don't use: "vimeo.com" in v.
    test = lambda v: "vimeo.com" == v
    parts = urlparse.urlparse(url)

    # vimeo.com has netloc='' and path='vimeo.com'
    if parts.netloc == '':
        return test(parts.path)
    else:
        return test(parts.netloc)

def vimeo_get_v(url):
    if not is_vimeo(url):
        raise InvalidVimeoLink(url)
    
    parts = urlparse.urlparse(url)
    
    # I can get parts.path == /12513670 or vimeo.com/12513670
    # either way, take what is after the / (all of the numbers)
    # I have also seen vimeo.com/8327882# or something similar
    r = re.search('(vimeo.com|)/(\d+)', parts.path)
 
    if r:
        return r.groups()[1]
    else:
        raise InvalidVimeoLink(repr(url) + " isn't a video url")

def vimeo_url_cleaner(url):
    if not is_vimeo(url):
        raise InvalidVimeoLink(url)
    
    return "http://vimeo.com/%s" % (vimeo_get_v(url))

def vimeo_clean_title(title):
    """
    Make the title more appealing, that is removes Vimeo branding and spacing
    """
    if title is None:
        return None

    t = re.sub(r"\s+on Vimeo", "", title)
    t = re.sub(r"\s+", " ", t)
    return t.strip()
