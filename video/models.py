# -*- coding: utf-8 -*-

from django.forms import ModelForm, CharField, TextInput, Textarea
from django.db import models
from django.utils import timezone
from random import choice

import urllib2
import tools

class UnusedManager(models.Manager):
    def get_query_set(self):
        return super(UnusedManager, self).get_query_set().filter(
            used__isnull=True)
    def random(self):
        q = super(UnusedManager, self).get_query_set().filter(
            used__isnull=True)
        if not q:
            return None
        return choice(q)
        
class UsedManager(models.Manager):
    def get_query_set(self):
        return super(UsedManager, self).get_query_set().filter(
            used__isnull=False)

# Create your models here.
class Video(models.Model):
    objects = models.Manager()
    used_objects = UsedManager()
    unused_objects = UnusedManager()


    title = models.CharField(blank=True,
                            max_length=1000,
                            help_text="Name of the video")
    url = models.URLField(max_length=1000,
                          unique=True,
                          help_text="URL to the video")
    used = models.DateTimeField(blank=True,
                                null=True,
                                default=None,
                                help_text="Track when you used this video last time")
    comment = models.TextField(blank=True,
                               help_text="Add text here to add some descrition to the video")

    def __unicode__(self):
        if self.title != "":
            return self.title
        else:
            return self.url

    
    def save(self, *args, **kwargs):
        """
        Before saving do the following:
        - Clean the url before saving
        - Fill in the name
        """

        # You can't timetravel into the future
        if self.used and self.used > timezone.now():
            self.used = timezone.now()

        if not self.url:
            raise ValueError("url field must be set")

        if not ("http://" in self.url or "https://" in self.url):
            self.url = "http://" + self.url

        # Note this may raise urllib2.URLException
        # if so, use the existing title
        try:
            self.title = tools.link_title(self.url)
        except urllib2.URLError:
            pass

        if tools.is_youtube(self.url):
            self.url = tools.youtube_url_cleaner(self.url)
            self.title = tools.youtube_clean_title(self.title)
        elif tools.is_vimeo(self.url):
            self.url = tools.vimeo_url_cleaner(self.url)
            self.title = tools.vimeo_clean_title(self.title)

        super(Video, self).save(*args, **kwargs)

class VideoForm(ModelForm):
    class Meta:
        model=Video
        fields=('url','comment',)

    url = CharField(
        label="Url till Youtube eller Vimeo:",
        widget=TextInput(attrs={'class':'input-block-level',
                                'placeholder':'URL till filmen'}))

    comment = CharField(
        label = 'Något att säga om filmen?',
        required = False,
        widget=Textarea(attrs={'class':'input-block-level',
                               'placeholder': "Skriv nå't om filmen"}))
