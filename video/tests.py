#-*- coding: utf-8 -*-
"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from video.models import Video
from urllib2 import URLError
from django.test import TestCase
from django.db import IntegrityError
from django.utils import timezone
from django.utils import timezone
from datetime import timedelta
import tools

class ModelTest(TestCase):
    def test_same_youtube_with_different_urls(self):
        """
        Tries to create a model object to the same youtube clip starting with
        slightly different urls. It should not work
        """
        v1 = Video(url="http://www.youtube.com/watch?v=iYHjpZe5NDY")
        v1.save()

        v2 = Video(url="http://youtu.be/iYHjpZe5NDY?t=2s")
        self.assertRaises(IntegrityError, v2.save)

    def test_used_in_the_future(self):
        """
        No quotes may be used in the future...
        Save a quote used in the future (1 day) and check if it is in the future
        """
        q = Video(url="http://www.youtube.com/watch?v=iYHjpZe5neh", used=timezone.now()+timedelta(days=1))
        q.save()

        # If used is in the future this will be negative
        dt = (timezone.now() - q.used).total_seconds()
        self.assertGreaterEqual(dt, 0,
                                msg="The video is %F sec in the future" % (dt*-1))

    def test_same_vimeo_with_different_urls(self):
        """
        Tries to create a model object to the same youtube clip starting with
        slightly different urls. It should not work
        """
        v1 = Video(url="http://vimeo.com/57370112")
        v1.save()

        v2 = Video(url="http://vimeo.com/57370112#")
        self.assertRaises(IntegrityError, v2.save)
        
    def test_url_required(self):
        """
        The url must be supplied
        """
        v1 = Video(title='c', used=timezone.now(), comment='c')
        self.assertRaises(ValueError, v1.save)

    def test_vimeo_get_v(self):
        """
        If all goes well we should get the video hash name from v=???
        """
        should_throw_exception = [
            "",
            "www.,google.com",
            None,
            "www.vimeo.com",
            "http://www.vimeo.com",
            "vimeo.com/",
            "http://www.vimeo.com/watch?annotation_id=annotation_742384&feature=iv&src_vid=tpNIgJQXPuM&w=yDS9MH1jRuw",
            "http://www.vimeo.com/watch?v=",
            ]
        
        for url in should_throw_exception:
            self.assertRaises(tools.InvalidVimeoLink,
                              tools.vimeo_get_v, url)
        clean_tests = [
            ("http://vimeo.com/57370112", "57370112"),
            ("http://vimeo.com/57370112watch?annotation_id=annotation_742384&feature=iv&src_vid=tpNIgJQXPuM&v=yDS9MH1jRuw","57370112"),
            ]

        for (url, correct)  in clean_tests:
            self.assertEqual(tools.vimeo_get_v(url), correct)

    def test_youtube_get_v(self):
        """
        If all goes well we should get the video hash name from v=???
        """
        should_throw_exception = [
            "",
            "www.,google.com",
            None,
            "www.youtube.com",
            "http://www.youtube.com",
            "youtube.com",
            "http://www.youtube.com/watch?annotation_id=annotation_742384&feature=iv&src_vid=tpNIgJQXPuM&w=yDS9MH1jRuw",
            "http://www.youtube.com/watch?v=",
            "http://www.youtube.com/watch?",
            "http://www.youtube.com/watch?v",
            "http://youtu.be",
            ]
        
        for url in should_throw_exception:
            self.assertRaises(tools.InvalidYouTubeLink,
                              tools.youtube_get_v, url)
        clean_tests = [
            ("http://www.youtube.com/watch?v=i9WvkSaR8Ho", "i9WvkSaR8Ho"),
            ("http://www.youtube.com/watch?annotation_id=annotation_742384&feature=iv&src_vid=tpNIgJQXPuM&v=yDS9MH1jRuw","yDS9MH1jRuw"),
            ("http://youtu.be/iYHjpZe5NDY?t=2s",'iYHjpZe5NDY'),
            ]

        for (url, correct)  in clean_tests:
            self.assertEqual(tools.youtube_get_v(url), correct)



    def test_url_title_correct(self):
        """
        Check if the url's name is filled in
        """
        v1 = Video(url='http://www.youtube.com/watch?v=KWJpKNQfXWo&list=FLoxcjq-8xIDTYp3uz647V5A')
        v1.save()

        self.assertEqual('Iodine Clock (slow motion) - Periodic Table of Videos', v1.title)

        v2 = Video(url="http://vimeo.com/57691682")
        v2.save()

        self.assertEqual("Cool Hunting Video Presents: The Planetarium Projector Museum", v2.title)

################################################################################
# TEST TOOLS.PY
################################################################################
class ToolsTest(TestCase):
    def test_link_title(self):
        """
        Downloads a known set of pages and tries to get the title.
        This requires Internet and that pages have the same title forever...
        This test was written 2013-01-16
        """

        title = tools.link_title
            
        # if someone registers this domain and answers on port 80,
        # I am going to kill that someone...slowly
        self.assertRaises(URLError, title, "http://jfkldsjfldsjflkdsjf.com")
        self.assertRaises(AttributeError, title, None)
        value_exceptions = ['Malformed URL', ""]
        for url in value_exceptions:
            self.assertRaises(ValueError, title, url)

        urls = [
            ("http://www.youtube.com/watch?v=EjdnJ-Meu3I",
             "COME AT ME BRO! - Bully: Scholarship Edition - Part 1 - Lets Play / Playthrough / Walkthrough - YouTube"),
            ("http://www.gnu.org/licenses/lgpl.txt",
             "http://www.gnu.org/licenses/lgpl.txt"),
            ]

        for (url, page_title) in urls:
            self.assertEqual(title(url), page_title)


    def test_youtube_clean_title(self):
        """
        Youtube got strange and uggly title for my program. Clean it
        """
        titles = [
            ('Epsilon-delta definition of limits - YouTube', 'Epsilon-delta definition of limits'),
            ('Anders   banan - YouTube   ', 'Anders banan'),
            ('Pika Pika', 'Pika Pika'),
            ('   Fire   Frei   ', 'Fire Frei'),
            (None, None),
            ("",""),
            ]

        yct = tools.youtube_clean_title
        for (test, correct) in titles:
            self.assertEqual(yct(test), correct)

    def test_vimeo_clean_title(self):
        """
        Vimeo got strange and uggly title for my program. Clean it
        """
        titles = [
            ('Epsilon-delta definition of limits on Vimeo', 'Epsilon-delta definition of limits'),
            ('Anders   banan on Vimeo   ', 'Anders banan'),
            ('Pika Pika', 'Pika Pika'),
            ('   Fire   Frei   ', 'Fire Frei'),
            (None, None),
            ("",""),
            ]

        vct = tools.vimeo_clean_title
        for (test, correct) in titles:
            self.assertEqual(vct(test), correct)

    def test_youtube_url_cleaner(self):
        """
        Cleans youtube urls and make sure that they are perfect.
        """
        should_throw_exception = [
            "",
            "www.,google.com",
            None,
            "www.youtube.com",
            "http://www.youtube.com",
            "youtube.com",
            "http://www.youtube.com/watch?annotation_id=annotation_742384&feature=iv&src_vid=tpNIgJQXPuM&w=yDS9MH1jRuw",
            "http://www.youtube.com/watch?v=",
            "http://www.youtube.com/watch?",
            "http://www.youtube.com/watch?v",
            "http://youtu.be",
            ]
        
        for url in should_throw_exception:
            self.assertRaises(tools.InvalidYouTubeLink,
                              tools.youtube_url_cleaner, url)
        clean_tests = [
            ("http://www.youtube.com/watch?v=i9WvkSaR8Ho", "http://www.youtube.com/watch?v=i9WvkSaR8Ho"),
            ("http://www.youtube.com/watch?annotation_id=annotation_742384&feature=iv&src_vid=tpNIgJQXPuM&v=yDS9MH1jRuw","http://www.youtube.com/watch?v=yDS9MH1jRuw"),
            ("http://youtu.be/iYHjpZe5NDY?t=2s",'http://www.youtube.com/watch?v=iYHjpZe5NDY'),
            ]

        for (url, correct)  in clean_tests:
            self.assertEqual(tools.youtube_url_cleaner(url), correct)
    

    def test_youtube_url_cleaner(self):
        """
        Cleans Vimeo urls and make sure that they are perfect.
        """
        should_throw_exception = [
            "",
            "www.,google.com",
            None,
            "www.vimeo.com",
            "http://www.vimeo.com",
            "vimeo.com",
            "http://www.vimeo.com/watch?annotation_id=annotation_742384&feature=iv&src_vid=tpNIgJQXPuM&w=yDS9MH1jRuw",
            "http://www.vimeo.com/watch?v=9349994",
            "http://www.vimeo.com/watch?",
            "http://www.vimeo.com/watch?v",
            ]
        
        for url in should_throw_exception:
            self.assertRaises(tools.InvalidVimeoLink,
                              tools.vimeo_url_cleaner, url)
        clean_tests = [
            ("http://vimeo.com/57691682", "http://vimeo.com/57691682"),
            ("http://vimeo.com/57691682watch?annotation_id=annotation_742384&feature=iv&src_vid=tpNIgJQXPuM&v=yDS9MH1jRuw","http://vimeo.com/57691682"),
            ]

        for (url, correct)  in clean_tests:
            self.assertEqual(tools.vimeo_url_cleaner(url), correct)

    def test_is_youtube(self):
        """
        Tests if the is_youtube function really returns true on various links
        """
        truly_youtube = [
            "http://www.youtube.com",
            "www.youtube.com",
            "youtube.com",
            "http://www.youtube.com/watch?annotation_id=annotation_742384&feature=iv&src_vid=tpNIgJQXPuM&v=yDS9MH1jRuw",
            "https://www.youtube.com",
            "http://www.youtube.com/watch?v=i9WvkSaR8Ho"
            "http://www.youtube.com/playlist?annotation_id=annotation_359763&feature=iv&list=PLYH8WvNV1YEmgZ7obSCm4Op4bv3j_6QYO&src_vid=i9WvkSaR8Ho",
            "http://www.youtube.com/?gl=SE&hl=sv",
            ]

        not_youtube = [
            "Kristina är en fin Kvinna",
            "",
            None,
            "http://www.google.com",
            "http://example.com/youtube.com",
            "http://www.somewhere.se/http://www.youtube.com",
            "http://somthing.youtube.com",
            ]


        test = tools.is_youtube
        for u in truly_youtube:
            self.assertTrue(test(u), 'Sent youtube link, got False: ' + u)

        for u in not_youtube:
            if u is None:
                self.assertFalse(test(u),
                                'Sent None, got True back...')
                continue
            self.assertFalse(test(u), 'False positive youtube link: ' + u)

        
    def test_is_vimeo(self):
        """
        Tests if the is_vimeo function really returns true on various links
        """
        truly_vimeo = [
            "http://vimeo.com",
            "vimeo.com",
            "vimeo.com",
            "http://vimeo.com/watch?annotation_id=annotation_742384&feature=iv&src_vid=tpNIgJQXPuM&v=yDS9MH1jRuw",
            "https://vimeo.com/",
            "http://vimeo.com/watch?v=i9WvkSaR8Ho"
            "http://vimeo.com/playlist?annotation_id=annotation_359763&feature=iv&list=PLYH8WvNV1YEmgZ7obSCm4Op4bv3j_6QYO&src_vid=i9WvkSaR8Ho",
            "http://vimeo.com/?gl=SE&hl=sv",
            ]

        not_vimeo = [
            "Kristina är en fin Kvinna",
            "",
            None,
            "http://www.google.com",
            "http://example.com/vimeo.com",
            "http://www.somewhere.se/http://vimeo.com",
            "http://somthing.vimeo.com",
            ]


        test = tools.is_vimeo
        for u in truly_vimeo:
            self.assertTrue(test(u), 'Sent vimeo link, got False: ' + u)

        for u in not_vimeo:
            if u is None:
                self.assertFalse(test(u),
                                'Sent None, got True back...')
                continue
            self.assertFalse(test(u), 'False positive vimeo link: ' + u)
