from django.contrib import admin
from video.models import Video
from django.utils import timezone

def marked_used(modeladmin, request, queryset):
    """Admin Action: Mark all selected videos as used by setting todays date and time"""

    queryset.update(used=timezone.now())
marked_used.short_description = "Mark as used today"

def marked_unused(modeladmin, request, queryset):
    """Admin Action: Unmark all selected videos by setting the used date to None"""
    
    queryset.update(used=None)
marked_unused.short_description = "Pretend these weren't used"

class VideoAdmin(admin.ModelAdmin):
    # No Need to go overboard on all fields to display
    list_display = ('title','used',)

    # One might want to sort on title and used date
    list_filter = ('title','used',)

    # We want to search a video title and url, don't we
    search_fields = ('title','url')

    # Actions on the videos, we want to select them as used and unused
    actions = (marked_used,marked_unused,)

admin.site.register(Video, VideoAdmin)
